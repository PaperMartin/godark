using Godot;
using System;
using System.IO;

[Tool]
public partial class FilePicker : Control
{
    private enum FilePickerModes
    {
        OpenFile,
        OpenFolder
    }


    [Export]
    private string _fieldName
    {
        get => _fieldNameInternal;
        set
        {
            _fieldNameInternal = value;
            if (_filePathField != null) _fieldNameLabel.Text = value;
        }
    }

    [Export] private FilePickerModes _filePickerMode;

    private Label _fieldNameLabel;
    private LineEdit _filePathField;
    private Button _filePickButton;
    private FileDialog _filePickDialog;
    private string _fieldNameInternal;


    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        _fieldNameLabel = GetNode<Label>("FieldName");
        _filePathField = GetNode<LineEdit>("FilePath");
        _filePickButton = GetNode<Button>("PickFileButton");
        _filePickDialog = GetNode<FileDialog>("FileDialog");

        _fieldNameLabel.Text = _fieldName;

        if (Engine.IsEditorHint())
        {
            return;
        }

        switch (_filePickerMode)
        {
            case FilePickerModes.OpenFile:
                _filePickDialog.FileMode = FileDialog.FileModeEnum.OpenFile;
                break;
            case FilePickerModes.OpenFolder:
                _filePickDialog.FileMode = FileDialog.FileModeEnum.OpenDir;
                break;
        }

        _filePickButton.Pressed += OnFilePickButtonPressed;

        _filePickDialog.FileSelected += OnFileDialogPathPicked;
        _filePickDialog.DirSelected += OnFileDialogPathPicked;

        _filePathField.TextSubmitted += OnFilePathFieldTextChanged;
    }

    private void OnFilePickButtonPressed()
    {
        _filePickDialog.PopupCentered();
    }

    private void OnFileDialogPathPicked(string path)
    {
        _filePathField.Text = path;
        EmitSignal(SignalName.OnPathPicked, path);
    }

    private void OnFilePathFieldTextChanged(string path)
    {
    }

    public void SetRootSubfolder(string path)
    {
        _filePickDialog.RootSubfolder = path;
    }

    [Signal]
    public delegate void OnPathPickedEventHandler(string path);
}