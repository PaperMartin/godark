﻿using System;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using Pfim;
using ImageFormat = Pfim.ImageFormat;

namespace GoDark.conversion.image;

public static class ImageConversionUtil
{
    public static byte[] DDSToPNG(byte[] dds)
    {
        var image = Pfimage.FromStream(new MemoryStream(dds));
        PixelFormat format;

        switch (image.Format)
        {
            case ImageFormat.Rgba32:
                format = PixelFormat.Format32bppArgb;
                break;
            case ImageFormat.Rgb24:
                format = PixelFormat.Format24bppRgb;
                break;
            default:
                throw new NotImplementedException("Format not yet supported : " + image.Format);
        }

        var handle = GCHandle.Alloc(image.Data, GCHandleType.Pinned);

        var data = Marshal.UnsafeAddrOfPinnedArrayElement(image.Data, 0);
        var bitmap = new Bitmap(image.Width, image.Height, image.Stride, format, data);
        MemoryStream returnStream = new();
        bitmap.Save(returnStream, System.Drawing.Imaging.ImageFormat.Png);
        return returnStream.ToArray();
    }
}