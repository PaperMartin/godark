using Godot;
using System;
using System.IO;

public partial class ModelPreviewInterface : Node
{
	[Export]
	private DebugModelPreview _previewNode;

	
	private Button _dsrInstallPathButton;
	private FileDialog _dsrInstallPathDialog;

	private Button _mtdPathButton;
	private FileDialog _mtdPathDialog;

	private Button _modelPathButton;
	private FileDialog _modelPathDialog;

	private Button _loadModelButton;
	
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
        GD.Print(OS.GetExecutablePath());
		_dsrInstallPathButton = GetNode<Button>("%SetDSRInstallPathButton");
		_dsrInstallPathDialog = GetNode<FileDialog>("%DSRInstallPathDialog");
		_dsrInstallPathButton.Pressed += OnDSRInstallPathButtonPressed;
		_dsrInstallPathDialog.DirSelected += OnDSRInstallPathSelected;
		
		_mtdPathButton = GetNode<Button>("%SetMTDPathButton");
		_mtdPathDialog = GetNode<FileDialog>("%MTDPathDialog");
		_mtdPathButton.Pressed += OnMTDFilePathButtonPressed;
		_mtdPathDialog.FileSelected += OnMTDFileSelected;
		_mtdPathButton.Disabled = true;
		

		_modelPathButton = GetNode<Button>("%SetModelPathButton");
		_modelPathDialog = GetNode<FileDialog>("%ModelPathDialog");
		_modelPathButton.Pressed += OnModelFilePathButtonPressed;
		_modelPathDialog.FileSelected += OnModelFileSelected;
		_modelPathButton.Disabled = true;

		_loadModelButton = GetNode<Button>("%LoadModelButton");
		_loadModelButton.Pressed += OnLoadModelButtonPressed;
		_loadModelButton.Disabled = true;


	}

	private void OnDSRInstallPathButtonPressed()
	{
		_dsrInstallPathDialog.PopupCentered();
	}
	private void OnDSRInstallPathSelected(string path)
	{
		//GD.Print("File path selected : " + path);
		_previewNode.SetInstallPath(path);
		_mtdPathDialog.RootSubfolder = path;
		_modelPathDialog.RootSubfolder = path;
		_mtdPathButton.Disabled = false;
		_modelPathButton.Disabled = false;
	}

	
	private void OnMTDFilePathButtonPressed()
	{
		_mtdPathDialog.PopupCentered();
	}
	private void OnMTDFileSelected(string path)
	{
		_previewNode.SetMTDPath(path);
	}
	
	
	private void OnModelFilePathButtonPressed()
	{
		_modelPathDialog.PopupCentered();
	}
	private void OnModelFileSelected(string path)
	{
		_previewNode.SetModelPath(path);
		_loadModelButton.Disabled = false;
	}

	private void OnLoadModelButtonPressed()
	{
		_previewNode.ImportMesh();
	}
}
