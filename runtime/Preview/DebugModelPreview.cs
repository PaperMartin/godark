using Godot;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.InteropServices;
using DirectXTexNet;
using GoDark.conversion.image;
using Pfim;
using SoulsFormats;
using SoulsFormats.Kuon;
using Image = Godot.Image;

public partial class DebugModelPreview : Node
{
	[Export(PropertyHint.GlobalDir)] private string _dsrInstallPath;
	[Export(PropertyHint.GlobalFile)] private string _dsrMTDPath;
	[Export(PropertyHint.GlobalFile)] private string _dsrModelPath;
	[Export] private MeshInstance3D _meshInstance3D;

	[ExportGroup("Mesh")]
	[Export] private bool _forceLod;
	[Export] private int _lod;

	[Export] private Godot.Collections.Dictionary<string, ImageTexture> _textures = new();

	[Export] private Godot.Collections.Dictionary<string, Material> _materials = new();

	private ArrayMesh _convertedMesh;
	private List<MTD> _mtds = new();
	private List<TPF> _tpfs = new();
	private List<FLVER2> _flver2s = new();

	public void SetInstallPath(string path)
	{
		_dsrInstallPath = path;
	}

	public void SetMTDPath(string path)
	{
		_dsrMTDPath = path;
	}

	public void SetModelPath(string path)
	{
		_dsrModelPath = path;
		//ImportMesh();
	}

	public override void _Ready()
	{
		//ImportMesh();
	}

	// Called when the node enters the scene tree for the first time.
	public void ImportMesh()
	{
		_mtds.Clear();
		_tpfs.Clear();
		_flver2s.Clear();
		//GD.Print(_dsrModelPath);

		List<BND3> bnds = new List<BND3>();
		List<BXF3> bxf3s = new List<BXF3>();
		//bnds.Add(BND3.Read(_dsrMTDPath));
		if (BND3.Is(_dsrModelPath))
		{
			bnds.Add(BND3.Read(_dsrModelPath));
		}
		if (FLVER2.Is(_dsrModelPath))
		{
			_flver2s.Add(FLVER2.Read(_dsrModelPath));
		}
		if (TPF.Is(_dsrModelPath))
		{
			_tpfs.Add(TPF.Read(_dsrModelPath));
		}

		{
			//bad
			string bdtpath = "";
			string bhdpath = "";
			bool isbdtorbhd = false;
			if (BXF3.IsBDT(_dsrModelPath))
			{
				isbdtorbhd = true;
				bdtpath = _dsrModelPath;
				bhdpath = bdtpath.Substring(0, bdtpath.Length - 3);
				bhdpath += "bhd";
			}
			else if (BXF3.IsBHD(_dsrModelPath))
			{
				isbdtorbhd = true;
				bhdpath = _dsrModelPath;
				bdtpath = bhdpath.Substring(0, bhdpath.Length - 3);
				bdtpath += "bdt";
			}

			if (isbdtorbhd)
			{
				bxf3s.Add(BXF3.Read(bhdpath,bdtpath));
			}
			
		}

		foreach (BND3 nBnd in bnds)
		{
			ProcessBnd3(nBnd);
		}

		foreach (BXF3 bxf3 in bxf3s)
		{
			ProcessBxf3(bxf3);
		}

		foreach (MTD mtd in _mtds)
		{
			//GD.Print("Current MTD shader : " + mtd.ShaderPath);
		}

		foreach (TPF tpf in _tpfs)
		{
			ProcessTextureContainer(tpf);
		}

		foreach (FLVER2 flver2 in _flver2s)
		{
			ProcessModel(flver2);
		}
	}

	private void ProcessBnd3(BND3 nBnd)
	{
		GD.Print("BND3 format : " + nBnd.Format);
		for (int i = 0; i < nBnd.Files.Count; i++)
		{
			GD.Print("File name : " + nBnd.Files[i].Name);
			byte[] file = nBnd.Files[i].Bytes;

			ProcessFile(file);
		}
	}
	
	private void ProcessBxf3(BXF3 bxf3)
	{
		GD.Print("BXF3 format : " + bxf3.Format);
		for (int i = 0; i < bxf3.Files.Count; i++)
		{
			GD.Print("File name : " + bxf3.Files[i].Name);
			byte[] file = bxf3.Files[i].Bytes;

			ProcessFile(file);
		}
	}
	

	private void ProcessFile(byte[] file)
	{
		if (MTD.Is(file))
		{
			MTD materialDescription = MTD.Read(file);
			_mtds.Add(materialDescription);
		}

		if (TPF.Is(file))
		{
			TPF textureContainer = TPF.Read(file);
			_tpfs.Add(textureContainer);
		}

		if (FLVER2.Is(file))
		{
			FLVER2 model = FLVER2.Read(file);
			_flver2s.Add(model);
		}
	}

	private void ProcessModel(FLVER2 model)
	{
		//GD.Print("Mesh count : " + model.Meshes.Count);
		//GD.Print("Bone count : " + model.Bones.Count);
		//GD.Print("Material count : " + model.Materials.Count);

		for (int currentMaterialId = 0; currentMaterialId < model.Materials.Count; currentMaterialId++)
		{
			ProcessMaterial(model.Materials[currentMaterialId]);
		}

		_convertedMesh = new ArrayMesh();

		for (int currentMeshId = 0; currentMeshId < model.Meshes.Count; currentMeshId++)
		{
			SoulsFormats.FLVER2.Mesh mesh = model.Meshes[currentMeshId];

			//GD.Print("Vertex count : " + mesh.Vertices.Count);
			//GD.Print("Faceset count : " + mesh.FaceSets.Count);

			SurfaceTool sfTool = new SurfaceTool();

			int startingFaceSet = _forceLod ? _lod : 0;
			int maxFaceSets = _forceLod ? _lod + 1 : mesh.FaceSets.Count;

			if (maxFaceSets > mesh.FaceSets.Count || maxFaceSets < 1)
			{
				continue;
			}


			sfTool.Begin(Mesh.PrimitiveType.Triangles);

			for (int currentVertexId = 0; currentVertexId < mesh.Vertices.Count; currentVertexId++)
			{
				FLVER.Vertex vertex = mesh.Vertices[currentVertexId];
				//GD.Print("Vertex Position : " + vertex.Position);
				//GD.Print("Vertex Normal : " + vertex.Normal);

				Vector3 gdVertexNormal = new Vector3(vertex.Normal.X, vertex.Normal.Y, vertex.Normal.Z);
				Vector3 gdVertexPosition = new Vector3(vertex.Position.X, vertex.Position.Y, vertex.Position.Z);
				Vector2 gdVertexUVs = new Vector2(vertex.UVs[0].X, vertex.UVs[0].Y);

				Color gdVertexColor = new Color
				{
					R = (gdVertexNormal.X * 0.5f) + 1,
					G = (gdVertexNormal.Y * 0.5f) + 1,
					B = (gdVertexNormal.Z * 0.5f) + 1,
					A = 1
				};

				sfTool.SetNormal(gdVertexNormal);
				sfTool.SetColor(gdVertexColor);
				sfTool.SetUV(gdVertexUVs);
				sfTool.AddVertex(gdVertexPosition);
			}

			for (int currentFaceSetId = startingFaceSet; currentFaceSetId < maxFaceSets; currentFaceSetId++)
			{
				FLVER2.FaceSet faceSet = mesh.FaceSets[currentFaceSetId];
				List<int> indices = faceSet.Triangulate(true, false);

				//GD.Print("Triangle indices count : " + indices.Count);

				for (int currentTriangleId = 0; currentTriangleId < indices.Count; currentTriangleId++)
				{
					//GD.Print(indices[i]);
					sfTool.AddIndex(indices[currentTriangleId]);
				}
			}

			sfTool.SetMaterial(_materials[model.Materials[mesh.MaterialIndex].Name]);
			sfTool.Commit(_convertedMesh);
		}

		_meshInstance3D.Mesh = _convertedMesh;
	}

	private void ProcessMaterial(FLVER2.Material material)
	{
		GD.Print("Material name : " + material.Name);
		

		StandardMaterial3D gdCurrentMaterial = new StandardMaterial3D();
		gdCurrentMaterial.CullMode = BaseMaterial3D.CullModeEnum.Disabled;

		foreach (FLVER2.Texture currentTexture in material.Textures)
		{
			GD.Print("Current Material Texture Type : " + currentTexture.Type);
			GD.Print("Current Material Texture Path : " + currentTexture.Path);
			string fileName = Path.GetFileNameWithoutExtension(currentTexture.Path);
			GD.Print("Current Material Texture File Name : " + fileName);
			//GD.Print("Current Material Texture Scale : " + currentTexture.Scale);

			ImageTexture gdCorrespondingTexture;
			bool bGotTexture = _textures.TryGetValue(fileName, out gdCorrespondingTexture);
			if (!bGotTexture)
			{
				GD.Print("Texture missing");
				continue;
			}

			switch (currentTexture.Type)
			{
				case "g_Diffuse":
					gdCurrentMaterial.AlbedoTexture = gdCorrespondingTexture;
					//gdCurrentMaterial.Transparency = BaseMaterial3D.TransparencyEnum.Alpha;
					break;
				case "g_Specular":
					//gdCurrentMaterial.MetallicTexture = gdCorrespondingTexture;
					//gdCurrentMaterial.Metallic = 1;
					//gdCurrentMaterial.MetallicSpecular = 1;
					
					//gdCurrentMaterial.RoughnessTexture = gdCorrespondingTexture;
					//gdCurrentMaterial.RoughnessTextureChannel = BaseMaterial3D.TextureChannel.Green;
					//gdCurrentMaterial.Roughness = 0;
					
					break;
				case "g_Bumpmap":
					gdCurrentMaterial.NormalEnabled = true;
					gdCurrentMaterial.NormalTexture = gdCorrespondingTexture;
					gdCurrentMaterial.HeightmapFlipBinormal = true;
					break;
			}
		}

		if (_materials.ContainsKey(material.Name))
		{
			GD.Print("Material already loaded : " + material.Name);
			_materials[material.Name] = gdCurrentMaterial;
		}
		else 
		{
			_materials.Add(material.Name, gdCurrentMaterial);
		}
	}

	private void ProcessTextureContainer(TPF textureContainer)
	{
		GD.Print("Texture count : " + textureContainer.Textures.Count);
		for (int currentTextureId = 0; currentTextureId < textureContainer.Textures.Count; currentTextureId++)
		{
			unsafe
			{
				TPF.Texture currentTexture = textureContainer.Textures[currentTextureId];
				GD.Print("Texture name : " + currentTexture.Name);

				Dds dds = Dds.Create(new MemoryStream(currentTexture.Bytes), new PfimConfig());

				if (dds.Compressed)
				{
					dds.Decompress();
				}

				int width = dds.Width;
				int height = dds.Height;
				
				GD.Print("Texture resolution : " + width + "x" + height);
				GD.Print("Texture format : " + dds.Format);
				//GD.Print("Texture mip count : " + dds.MipMaps.Length);
				//GD.Print("Texture Data Length : " + dds.DataLen);
				//GD.Print("Texture Byte per pixel : " + dds.BytesPerPixel);

				Image gdImage = new Image();

				byte[] pngImage = ImageConversionUtil.DDSToPNG(currentTexture.Bytes);

				gdImage.LoadPngFromBuffer(pngImage);
				//gdImage.SetData(width, height, false, gdImageFormat, imageDataWithoutHeader);

				ImageTexture gdImageTexture = new ImageTexture();
				gdImageTexture.SetImage(gdImage);
				gdImageTexture.ResourceName = currentTexture.Name;

				if (!_textures.TryAdd(currentTexture.Name, gdImageTexture))
				{
					GD.Print("Texture already loaded : " + currentTexture.Name);
					_textures[currentTexture.Name] = gdImageTexture;
				}
			}
		}
	}
}
